package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CSDLListTester {

	public static void main(String[] args) {
		CSDLList<Car> list = new CSDLList<Car>(new CarComparator<Car>());
		Car car1 = new Car(1, "Toyota", "Rav4", "LE", 30000);
		Car car2 = new Car(2, "Toyota", "Rav4", "SE", 30000);
		Car car3 = new Car(3, "Honda", "RS", "X", 30000);
		/*
		 * Testing add method.
		 */
		System.out.println(list.add(car1));
		System.out.println(list.add(car2));
		System.out.println(list.add(car3));
		System.out.println(list.add(car3));
		System.out.println(list.add(car3));
		System.out.println(list.add(car3));
		System.out.println(list.add(car3));
//		System.out.println(list.toString());
		
//		/*
//		 * Testing iterator.
//		 */
//		Iterator carIt = list.iterator();
//		while(carIt.hasNext()) {
//			System.out.println(carIt.next());
//		}
//		
//		/*
//		 * Testing remove methods.
//		 */
		System.out.println(list.remove(0));
		System.out.println(list.toString());
		
		System.out.println(list.remove(4));
		System.out.println(list.toString());
//		
//		System.out.println(list.remove(car1));
//		System.out.println(list.toString());
//		
//		System.out.println(list.removeAll(car3));
//		System.out.println(list.toString());
//		
//		/*
//		 * Testing clear method.
//		 */
//		list.clear();
//		System.out.println(list.toString());
//		
//		/*
//		 * Testing false remove methods.
//		 */
//		System.out.println(list.remove(car1));
//		System.out.println(list.remove(0));
//		System.out.println(list.removeAll(car3));
		
		Car[] carArr = list.toArray(new Car[0]);
//		System.out.println(carArr);
		for(int i = 0; i<carArr.length; i++) {
			System.out.println("Car: " + i + ": " + carArr[i].toString());
		}
		
	}

}
