package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CSDLList;

/**
 * CarManager class that creates a list of Cars using CSDLList and manages REST operations based on a car dealership.
 * @author Fernando
 *
 */
@Path("/cars")
public class CarManager {
	private static final CSDLList<Car> carList = new CSDLList<Car>(new CarComparator<Car>());

	/**
	 * Gets all cars in the list.
	 * 
	 * @return array - array containing all cars in the list.
	 */
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		return carList.toArray(new Car[0]);
	}

	/**
	 * Gets the car in the id given.
	 * 
	 * @param id - id of car.
	 * @return car - if not found returns NotFoundException, else returns car.
	 */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for (int i = 0; i < carList.size(); i++) {
			if (carList.get(i).getCarId() == id)
				return carList.get(i);
		}
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}

	/**
	 * Adds the car to the list.
	 * 
	 * @param car - car to be added.
	 * 
	 * @return if car could be added.
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		carList.add(car);
		return Response.status(201).build();
	}

	/**
	 * Updates the car of given id..
	 * 
	 * @param car - car with the updated information.
	 * 
	 * @return if car could be updated successfully, or if not found.
	 */
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for (int i = 0; i < carList.size(); i++) {
			if (car.getCarId() == carList.get(i).getCarId()) {
				carList.remove(i);
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	/**
	 * Delets the car of given id.
	 * 
	 * @param id - id of car to be deleted.
	 * 
	 * @return if car could be deleted successfully, or if not found.
	 */
	@DELETE
	@Path("{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		for (int i = 0; i < carList.size(); i++) {
			if (id == carList.get(i).getCarId()) {
				carList.remove(carList.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}
}
