package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Arrays;

public class CarComparatorTest {

	public static void main(String[] args) {
		Car car1 = new Car(1, "Toyota", "Rav4", "LE", 30000);
		Car car2 = new Car(2, "Toyota", "Rav4", "SE", 30000);
		Car car3 = new Car(3, "Honda", "RS", "X", 30000);
		Car[] cars = {car2, car1, car3};
		
		for(int i = 0; i<3; i++) {
			System.out.print(cars[i].getCarBrand() + cars[i].getCarModel() + cars[i].getCarModelOption() + " ");
		}
		System.out.println();
		
		Arrays.sort(cars, new CarComparator());
		
		for(int i = 0; i<3; i++) {
			System.out.print(cars[i].getCarBrand() + cars[i].getCarModel() + cars[i].getCarModelOption() + " ");
		}
		System.out.println();
	}
}
